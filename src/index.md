---
layout: layouts/page.njk
title: Intégration et documents Web
---

Ce cours fait partie du programme de la licence progessionnelle [AMIO](amio) 

## SVG

### Séance 1

- Introduction au format [SVG](svg)
- Dessiner des [formes](svg/formes)
- [Propriétés](svg/propriétés) des éléments graphiques.
- [Utilisation](svg/utilisation) des illustrations SVG avec les pages web.


### Séance 2
- Utilisation de portions de fichier SVG en utilisant les [fragments](svg/fragment).
- [Zone](svg/zone)
- [Bibliothèque](svg/bibliotheque)
- [Découpe et masquage](svg/découpe)


### Séance 3
- [Texte](svg/texte)
- [Transformation](svg/transformation)
- [Remplissage](svg/couleur) en dégradé et avec motifs 

### Séance 4

- [Animation](svg/animation)
- [Filtres](svg/filtres)

[Interaction en javascript](svg/javascript)

### Exercices

1a- Les [formes](svg/formes/exercice)

1b- Le [découpage](svg/formes/exercice/tv)

2a- Colorier une [carte](svg/exercices/carte)

2b- Créer un [plan](svg/exercices/plan)

3a - Créer un [logo](svg/exercices/logo) et le décliner en plusieurs couleurs

3b - Colorier un [symbole](svg/exercices/guitare)

4b- [Animer](svg/exercices/vague) avec fond de page

5a- [Interagir](svg/exercices/cuve) avec un illustration

6a- Les [filtres](svg/exercices/vision) SVG dynamiques

6b- Représenter un objet en 3D isométrique

7a - [Horloge](svg/exercices/horloge)

7b - [Atelier](svg/exercices/atelier)

## Javascript

- Les [tableaux](javascript/tableaux)
- Les API [matérielles](javascript/materiel)
- Les [sélecteurs](javascript/selecteur) du DOM
- Programmation [asynchrone](javascript/asynchrone)

1a- [Sélection](svg/exercices/carte) suivant un critère 

1b- [Map Filter Reduce](javascript/tableaux/exercice)

2a- Les [sélecteurs](javascript/selecteur/exercice) du DOM

3a - [Carte](javascript/exercices/carte)

3b - [Chargement](javascript/exercices/chargement)

3c - [PDF](javascript/exercices/pdf)

### Exercices

1. [Travailler avec des données](html/examen1/)

2. [Manipuler un tableau](html/examen2/)

## HTML

[HTML](html)

[CSS](css)

[Javascript](javascript)

[PDF](pdf)


## Exercices

### Exercice 1

Voir l'[énoncé](javascript/exercice)

Compétences évaluées : SVG / Fetch / Tableaux
