---
layout: layouts/page.njk
title: HTML
---

[Listes](liste)

[Fullscreen](fullscreen)

## Formulaires

[Formulaire](formulaire)

[Pattern](formulaire/pattern)

[Tableau](tableau)

[Interactif](interactif)

[Microdata](microdata)

[Plein écran](fullscreen)

[Listes](liste)

[Unites](unites)
