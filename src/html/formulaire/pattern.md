<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>Formulaire</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="/assets/lib/highlight.js/default.css">
	<script src="/assets/lib/highlight.js/highlight.pack.js"></script>
    <script>hljs.initHighlightingOnLoad();</script>
</head>
<body>

<form action="" method="POST">

<pre><code>&lt;input type='text'&gt;</code></pre>
<input type='text'>

<pre><code>&lt;input type='text'&gt;</code></pre>
<input type='text' pattern='[0-9,]*'>

<pre><code>&lt;input type='text'&gt;</code></pre>
<input type='text' pattern='\d*'>

<pre><code>&lt;input type='text'&gt;</code></pre>
<input type="text" name="country_code" pattern="[A-Za-z]{3}" title="Three letter country code">


</form>

</body>
</html>
