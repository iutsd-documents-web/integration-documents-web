---
layout: layouts/page.njk
title: PDF
---

Lecteur PDF
<div id="mypdf">
<iframe src="/cennik.pdf#zoom=65" style="width: 100%; height: 800px;" frameborder="0" scrolling="no">
        <p>Your web browser doesn't support iframes.</p>
   </iframe>
</div>

#navpanes=1&pagemode=bookmarks&view=fitH
    view: implemented (accepts Fit, FitH, FitV - for vertical resp horizontal fit)
    toolbar: implemented (hides top-bar, but not zoom-buttons bottom right)
    zoom: implemented
    scrollbar: not implemented
    page: implemented
    nameddest: implemented
    search: filed bug 792647 to track separately
    navpanes: does not apply
    statusbar: does not apply
