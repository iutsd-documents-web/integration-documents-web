---
layout: layouts/page.njk
date: Last Modified
title: Licence Professionnelle Applications Mobiles et Internet des Objets (AMIO)
---

### 501.2 : Intégration et documents web

Html / css / framework css / design adaptatif

[https://integration-documents-web.netlify.app/](https://integration-documents-web.netlify.app/)

**ECTS :** 4\
**Nombre d'heures :** 40\
**Enseignant :**

### 504.2 : Programmation pour systèmes embarqués

Programmation dédiée aux objets (multiprocessus, synchronisation, mémoire partagée

[https://programmation-embarquee.netlify.app/](https://programmation-embarquee.netlify.app/)

**ECTS :** 3\
**Nombre d'heures :** 30\
**Enseignant :**

### 601.1 : Technologies du web pour la mobilité

Développement d'application mobiles html5 (html/css/js et plateformes hybrides)

[https://technologies-web-mobilite.netlify.app/](https://technologies-web-mobilite.netlify.app/)

**ECTS :** 2\
**Nombre d'heures :** 30\
**Enseignant :** Emmanuel Medina

### 602.2 : Programmation mobile 1

Faire communiquer des applications, services, capteurs

Plateforme : Android avec WinDev Mobile

[https://programmation-mobile-1.netlify.app/](https://programmation-mobile-1.netlify.app/)

**ECTS :** 4\
**Nombre d'heures :** 40\
**Enseignant :** Emmanuel Medina
