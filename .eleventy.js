
module.exports = function (config) {

  config.addPassthroughCopy("src/**/*.png");
  config.addPassthroughCopy("src/**/*.jpg");
  config.addPassthroughCopy("src/**/*.webp");
  config.addPassthroughCopy("src/**/*.svg");
  config.addPassthroughCopy("src/**/*.css");
   //config.addPassthroughCopy("src/**/*.sh");
  //config.addPassthroughCopy("src/**/*.service");
  //config.addPassthroughCopy("src/**/*.timer");

  const cleancss = require("clean-css");
  config.addFilter("cleancss", function (code) {
    return new cleancss({}).minify(code).styles;
  });

  const md = require("markdown-it")({
    html: true,
    linkify: false,
    typographer: false
  })
    .use(require("@gerhobbelt/markdown-it-attrs"))
    .use(require("markdown-it-deflist"));
    
  config.setLibrary("md", md);

  const syntaxHighlight = require("@11ty/eleventy-plugin-syntaxhighlight");
  config.addPlugin(syntaxHighlight);
  
  /*
  // Mathjax : pb with html
  const mathjaxPlugin = require("eleventy-plugin-mathjax");
  config.addPlugin(mathjaxPlugin);
*/

/*
const codeblocks = require('@code-blocks/eleventy-plugin');
const math = require('@code-blocks/math');

config.addPlugin(codeblocks([
  math
]))
*/
  const highlighter = config.markdownHighlighter;

  config.addMarkdownHighlighter((str, language) => {
    if (language === "mermaid") {
      return `<pre class="mermaid">${str}</pre>`;
    // https://cornishweb.com/index.php/2019/05/25/using-mermaid-js-with-eleventy-io/
    }
    return highlighter(str, language);
  });

  return {
    dir: {
      input: "src"
    }
  }

};
